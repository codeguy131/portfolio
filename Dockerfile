# start from an official image
FROM python:3.10-slim

RUN python -m pip install --upgrade pip

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

ENTRYPOINT ["sh", "./entrypoint.sh"]
