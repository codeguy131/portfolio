"""Models for projects"""
from django.db import models

# Create your models here.
class Project(models.Model):
    """DB model for projects"""

    name = models.CharField(max_length=32)
    repository = models.URLField()
    docs_page = models.URLField(null=True, blank=True)
    commits = models.IntegerField()
    description = models.TextField(max_length=500)
    first_commit = models.DateTimeField()
    image = models.ImageField(null=True, blank=True, upload_to="images")

    def __str__(self):
        return self.name.__str__()

    def placeholder1(self):
        """placeholder"""

    def placeholder2(self):
        """placeholder"""
