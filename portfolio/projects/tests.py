"""Project tests"""
from datetime import datetime
from django.test import TestCase, Client
from .models import Project

# Create your tests here.
class ProjectTests(TestCase):
    """Project test cases"""

    def setUp(self):
        proj = Project.objects.create(
            repository="https://gitlab.com",
            name="test_project",
            commits=12,
            first_commit=datetime.now(),
        )
        proj.save()
        return proj

    def test_project_create(self):
        """Check to make sure names are given correctly"""

        proj = Project.objects.get(name="test_project")
        self.assertTrue(proj.name == "test_project")
