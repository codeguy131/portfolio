"""Views for projects"""
from django.conf import settings
from django.views.generic import ListView
from projects.models import Project

# Create your views here.
class Index(ListView):
    """Index for Projects"""

    template_name = "projects/index.html"
    model = Project

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["main_project"] = Project.objects.get(name="T-CHAIN")
        context["secondary_project"] = Project.objects.get(name="Freeroast")
        context["my_name"] = settings.MY_NAME
        context["my_email"] = settings.MY_EMAIL
        context["my_gitlab"] = settings.MY_GITLAB
        context["my_linkedin"] = settings.MY_LINKEDIN
        context["my_pgp"] = settings.MY_PGP
        context["my_general_location"] = settings.MY_GENERAL_LOCATION
        return context
